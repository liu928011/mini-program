//获取应用实例
var app = getApp();

Page({
    data: {
        goods_list: [],
        default_address: {
            name: "",
            mobile: "",
            detail: "",
        },
        yun_price: "0.00",
        pay_price: "0.00",
        total_price: "0.00",
        note: '',
        params: null
    },
    onShow: function () {
        var that = this;
        that.getOrderInfo()
    },
    onLoad: function (e) {
        var that = this;
        that.setData({
            params: e.data
        });
        //console.log(that.data.params)
    },
    createOrder: function (e) {
        wx.showLoading();
        var that = this;
        wx.request({
            url: app.buildUrl('/order/create'),
            header: app.getRequestHeader(),
            method: 'POST',
            data: {
                'data': that.data.params,
                'note': that.data.note,
                'yun_price': that.data.yun_price,
                'default_address':JSON.stringify(that.data.default_address)
            },
            success(res) {
                wx.hideLoading();
                var resp = res.data;
                if (resp.code === 200) {
                    that.setData({});
                    wx.redirectTo({
                        url: "/pages/my/order_list"
                    });
                } else {
                    app.alert({'content': resp.msg})
                }
            }
        });
    },

    getInputValue: function (e) {
        this.data.note = e.detail['value']
    },
    addressSet: function () {
        wx.navigateTo({
            url: "/pages/my/addressSet"
        });
    },
    selectAddress: function () {
        wx.navigateTo({
            url: "/pages/my/addressList"
        });
    },
    getOrderInfo: function () {
        var that = this;
        wx.request({
            url: app.buildUrl('/order/info'),
            header: app.getRequestHeader(),
            method: 'POST',
            data: {'data': that.data.params},
            success(res) {
                var resp = res.data;
                if (resp.code === 200) {
                    that.setData({
                        goods_list: resp.data.goods_list,
                        yun_price: resp.data.yun_price,
                        pay_price: resp.data.pay_price,
                        total_price: resp.data.total_price,
                        default_address: resp.data.default_address //默认地址
                    });
                } else {
                    app.alert({'content': resp.msg})
                }
            }
        });
    }
});
