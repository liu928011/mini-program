//login.js
//获取应用实例
var app = getApp();
Page({
    data: {
        remind: '加载中',
        angle: 0,
        userInfo: {},
        hasUserInfo: false,
        canIUseGetUserProfile: true,
    },
    goToIndex: function () {
        wx.switchTab({
            url: '/pages/food/index',
        });
    },
    onLoad: function () {
        wx.setNavigationBarTitle({
            title: app.globalData.shopName
        });
        // 检查用户是否已经授权
        this.checkLogin()
    },
    onShow: function () {

    },
    onReady: function () {
        var that = this;
        setTimeout(function () {
            that.setData({
                remind: ''
            });
        }, 1000);
        wx.onAccelerometerChange(function (res) {
            var angle = -(res.x * 30).toFixed(1);
            if (angle > 14) {
                angle = 14;
            } else if (angle < -14) {
                angle = -14;
            }
            if (that.data.angle !== angle) {
                that.setData({
                    angle: angle
                });
            }
        });
    },
    getUserProfile(e) {
        var that = this;
        wx.getUserProfile({
            lang: "zh_CN", //设置获取用户信息为中文
            desc: '用于完善会员资料',
            success: (res) => {
                this.setData({
                    userInfo: res.userInfo,
                    hasUserInfo: true
                });
                if (!res.userInfo) {
                    app.alert({'content': '登入失败，请再次单击'});
                    return false
                }
                var data = res.userInfo;
                wx.login({
                    success(res) {
                        if (res.code) {
                            //发起网络请求
                            data['code'] = res.code;
                            wx.request({
                                url: app.buildUrl('/member/login'),
                                data: data,
                                method: 'POST',
                                success: function (res) {
                                    if (res.data.code != 200) {
                                        app.alert({'content': res.data.msg});
                                        return false
                                    }
                                    app.setCache('token', res.data.data.token);
                                    //app.alert({'content': res.data.msg,'cb_confirm':that.goToIndex});
                                    that.goToIndex() //授权成功，则跳转
                                }
                            })
                        } else {
                            app.alert({'content': '登入失败，请再次单击'});
                            return false
                        }
                    }
                })
            }
        })
    },
    checkLogin: function () {
        var that = this;
        wx.login({
            success(res) {
                if (res.code) {
                    wx.request({
                        url: app.buildUrl('/member/check-register'),
                        data: {'code': res.code},
                        method: 'POST',
                        success: function (res) {
                            if (res.data.code != 200) {
                                that.setData({
                                    canIUseGetUserProfile: false
                                });
                                return false
                            }
                            app.setCache('token', res.data.data.token);
                        }
                    })
                } else {
                    app.alert({'content': '登入失败，请再次单击'});
                    return false
                }
            }
        })

    }
});