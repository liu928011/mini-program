//获取应用实例
var app = getApp();
Page({
    data: {
        "content": "",
        "score": 10,
        "order_sn": ""
    },
    onLoad: function (e) {
        this.setData({
            'order_sn': e.order_sn
        })
    },
    scoreChange: function (e) {
        this.setData({
            "score": e.detail.value
        });
    },
    inputContent: function (e) {
        this.setData({
            content: e.detail.value
        })
    },
    doComment: function () {
        var that = this;
        wx.request({
            url: app.buildUrl("/my/comment/set"),
            header: app.getRequestHeader(),
            method: 'POST',
            data: {
                'content': that.data.content,
                'score': that.data.score,
                'order_sn': that.data.order_sn
            },
            success: function (res) {
                var resp = res.data;
                if (resp.code == 200) {
                    var param = {
                        'content': resp.msg,
                        'cb_confirm': function () {
                            wx.redirectTo({
                                url: "/pages/my/commentList"
                            })
                        }
                    };
                    app.alert(param);
                } else {
                    app.alert({"content": resp.msg})
                }

            }
        });
    }
});