//获取应用实例
var app = getApp();
Page({
    data: {},
    onLoad() {

    },
    onShow() {
        let that = this;
        this.getMyInfo();
    },
    getMyInfo: function () {
        var that = this;
        wx.request({
            url: app.buildUrl("/my/info"),
            header: app.getRequestHeader(),
            method: 'GET',
            success: function (res) {
                var resp = res.data;
                if (resp.code == 200) {
                    that.setData({
                        user_info:resp.data.user_info
                    })
                } else {

                }
            }
        });
    }

});