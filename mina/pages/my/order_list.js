var app = getApp();
Page({
    data: {
        statusType: ["待付款", "待发货", "待收货", "待评价", "已完成", "已关闭"],
        status: ["-8", "-7", "-6", "-5", "1", "0"],
        currentType: 0,
        tabClass: ["", "", "", "", "", ""]
    },
    statusTap: function (e) {
        var curType = e.currentTarget.dataset.index;
        this.data.currentType = curType;
        this.setData({
            currentType: curType
        });
        this.onShow();
    },
    orderDetail: function (e) {
        wx.navigateTo({
            url: "/pages/my/order_info?id="+ e.currentTarget.dataset.id
        })
    },
    onLoad: function (e) {
        // 生命周期函数--监听页面加载

    },
    onReady: function () {
        // 生命周期函数--监听页面初次渲染完
    },
    onShow: function () {
        var that = this;
        that.getOrderInfoList();
    },
    onHide: function () {
        // 生命周期函数--监听页面隐藏

    },
    onUnload: function () {
        // 生命周期函数--监听页面卸载

    },
    onPullDownRefresh: function () {
        // 页面相关事件处理函数--监听用户下拉动作

    },
    onReachBottom: function () {
        // 页面上拉触底事件的处理函数

    },
    getOrderInfoList: function () {
        var that = this;
        wx.request({
            url: app.buildUrl('/my/order'),
            header: app.getRequestHeader(),
            data: {
                'status': that.data.status[that.data.currentType]
            },
            success(res) {
                var resp = res.data;
                if (resp.code === 200) {
                    that.setData({
                        order_list: resp.data.order_list
                    });
                } else {
                    app.alert({'content': resp.msg})
                }
            }
        });
    },
    toPay: function (e) {
        var that = this;
        wx.request({
            url: app.buildUrl('/order/pay'),
            header: app.getRequestHeader(),
            method: 'POST',
            data: {
                'order_sn': e.currentTarget.dataset.id
            },
            success(res) {
                var resp = res.data;
                if (resp.code === 200) {
                    var pay_info = resp.data.pay_info;
                    wx.requestPayment({
                        'timeStamp': pay_info.timeStamp,
                        'nonceStr': pay_info.nonceStr,
                        'package': pay_info.package,
                        'signType': 'MD5',
                        'paySign': pay_info.paySign,
                        'success': function (res) {
                        },
                        'fail': function (res) {
                        }
                    });

                } else {
                    app.alert({'content': resp.msg})
                }
            }
        });
    },
    orderCancel: function (e) {
        this.orderOps(e.currentTarget.dataset.id, 'cancel', '确认取消订单吗？');
    },
    orderConfirm: function (e) {
        this.orderOps(e.currentTarget.dataset.id, 'confirm', '确认收货成功吗？');
    },
    orderOps: function (id, act, msg) {
        var that = this;
        var param = {
            'content': msg,
            'cb_confirm': function () {
                wx.request({
                    url: app.buildUrl('/order/ops'),
                    header: app.getRequestHeader(),
                    method: 'POST',
                    data: {
                        'order_sn': id,
                        'act': act
                    },
                    success(res) {
                        var resp = res.data;
                        if (resp.code === 200) {
                            if(resp.data.act == 'confirm'){
                                that.setData({
                                    currentType:3
                                })
                            }
                            that.onShow()

                        } else {
                            app.alert({'content': resp.msg})
                        }
                    }
                });
            }
        };
        app.tip(param)
    },
    toComment: function (e) {
        wx.navigateTo({
            url: "/pages/my/comment?order_sn=" + e.currentTarget.dataset.id
        })
    }
});
