//获取应用实例
var app = getApp();
Page({
    data: {
        addressList: []
    },
    selectTap: function (e) {
         var that = this;
          wx.request({
            url: app.buildUrl("/my/address/ops"),
            header: app.getRequestHeader(),
            method: 'POST',
            data:{
                'id':e.currentTarget.dataset.id,
                'act':'default'
            },
            success: function (res) {
                var resp = res.data;
                if (resp.code == 200) {
                    // 刷新页面
                    that.onShow()
                } else {
                    app.alert({"content": resp.msg})
                }

            }
        });

        //从商品详情下单选择地址之后返回
        //wx.navigateBack({});
    },
    addessSet: function (e) {
        wx.navigateTo({
            url: "/pages/my/addressSet?id=" + e.currentTarget.dataset.id
        })
    },
    onShow: function () {
        var that = this;
        this.getAddressListInfo();
    },
    getAddressListInfo:function () {
        var that = this;
          wx.request({
            url: app.buildUrl("/my/address"),
            header: app.getRequestHeader(),
            method: 'GET',
            success: function (res) {
                var resp = res.data;
                if (resp.code == 200) {
                    that.setData({
                         addressList:resp.data.addressList
                    })
                } else {
                    app.alert({"content": resp.msg})
                }

            }
        });
    }
});
