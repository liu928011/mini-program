var app = getApp();
Page({
    data: {},
    onLoad: function (e) {
        var that = this;
        that.setData({
            id: e.id
        })
    },
    onShow: function () {
        var that = this;
        that.getOrderInfoDetail();
    },
    getOrderInfoDetail: function () {
        var that = this;
        wx.request({
            url: app.buildUrl('/order/detail'),
            header: app.getRequestHeader(),
            method: 'GET',
            data: {'id': that.data.id},
            success(res) {
                var resp = res.data;
                if (resp.code === 200) {
                    that.setData({
                        info:resp.data.info
                    });
                } else {
                    app.alert({'content': resp.msg})
                }
            }
        });
    }
});