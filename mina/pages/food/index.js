//index.js
//获取应用实例
var app = getApp();
Page({
    data: {
        indicatorDots: true,
        autoplay: true,
        interval: 3000,
        duration: 1000,
        loadingHidden: false, // loading
        swiperCurrent: 0,
        categories: [],
        activeCategoryId: 0,
        goods: [],
        scrollTop: "0",
        loadingMoreHidden: true,
        searchInput: '',
        page: 1,
        processing: false
    },
    onShow: function () {
        this.getBannersAndCat();
    },
    onLoad: function () {
        var that = this;
        wx.setNavigationBarTitle({
            title: app.globalData.shopName
        });
    },
    scroll: function (e) {
        var that = this, scrollTop = that.data.scrollTop;
        that.setData({
            scrollTop: e.detail.scrollTop
        });
    },
    //事件处理函数
    swiperchange: function (e) {
        this.setData({
            swiperCurrent: e.detail.current
        })
    },
    listenerSearchInput: function (e) {
        this.setData({
            searchInput: e.detail.value
        });
    },
    toSearch: function (e) {
        this.setData({
            p: 1,
            goods: [],
            loadingMoreHidden: true
        });
        this.getFoodList();
    },
    tapBanner: function (e) {
        if (e.currentTarget.dataset.id != 0) {
            wx.navigateTo({
                url: "/pages/food/info?id=" + e.currentTarget.dataset.id
            });
        }
    },
    toDetailsTap: function (e) {
        wx.navigateTo({
            url: "/pages/food/info?id=" + e.currentTarget.dataset.id
        });
    },

    getBannersAndCat: function () {
        var that = this;
        wx.request({
            url: app.buildUrl('/food/index'),
            header: app.getRequestHeader(),
            success(res) {
                var resp = res.data;
                if (resp.code === 200) {
                    that.setData({
                        banners: resp.data.banners,
                        categories: resp.data.categories,
                    });
                    that.getFoodList()
                } else {
                    app.alert({'content': resp.msg})
                }
            }
        })
    },
    catClick: function (e) {
        // 当点击分类时，实现分类查询
        var that = this;
        that.setData({
            activeCategoryId: e.currentTarget.id,
            // 重置参数
            loadingMoreHidden: true,
            goods: [],
            page: 1,
        });
        that.getFoodList()
    },
    onReachBottom: function () {
        // 当向下拉到底部时，0.5秒的延时在发请求
        var that = this;
        if (that.data.loadingMoreHidden) {
            setTimeout(function () {
                that.getFoodList()
            }, 500)
        }
    },
    getFoodList: function () {
        var that = this;
        if (that.data.processing) {
            // 请求是否在进行中
            return false
        }
        that.setData({
            processing: true
        });
        wx.request({
            url: app.buildUrl('/food/search'),
            header: app.getRequestHeader(),
            data: {
                'cat_id': that.data.activeCategoryId,//分类查询
                'mix_kw': that.data.searchInput,//混合查询
                'page': that.data.page //分页
            },
            success(res) {
                var resp = res.data;
                if (resp.code === 200) {
                    var goods = resp.data.goods;
                    that.setData({
                        goods: that.data.goods.concat(goods),
                        page: that.data.page + 1, //发起二次请求页码加1
                        processing: false,
                    });
                    if (resp.data.has_more === 0) {
                        that.setData({
                            loadingMoreHidden: false
                        })
                    }
                } else {
                    app.alert({'content': resp.msg});
                    return false
                }
            }
        })
    }
});
