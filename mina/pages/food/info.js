//index.js
//获取应用实例
var app = getApp();
var WxParse = require('../../wxParse/wxParse.js');
var utils = require('../../utils/util.js');

Page({
    data: {
        autoplay: true,
        interval: 3000,
        duration: 1000,
        swiperCurrent: 0,
        hideShopPopup: true,
        buyNumber: 1,
        buyNumMin: 1,
        buyNumMax: 1,
        canSubmit: false, //  选中时候是否允许加入购物车
        shopCarInfo: {},
        shopType: "addShopCar",//购物类型，加入购物车或立即购买，默认为加入购物车,
        id: 0,
        shopCarNum: 0,
        commentCount: 2
    },
    onShow: function () {
        this.getFoodInfo()
    },
    onLoad: function (e) {
        var that = this;
        // 获取跳转的id
        that.setData({
            id: e.id
        });
    },
    goShopCar: function () {
        wx.switchTab({
            url: "/pages/cart/index"
        });
    },
    toAddShopCar: function () {
        this.setData({
            shopType: "addShopCar"
        });
        this.bindGuiGeTap();
    },
    tobuy: function () {
        this.setData({
            shopType: "tobuy"
        });
        this.bindGuiGeTap();
    },
    addShopCar: function () {
        var that = this;
        var number = that.data.buyNumber;
        var id = that.data.info.id;
        if (id && number) {
            wx.request({
                url: app.buildUrl('/cart/add'),
                header: app.getRequestHeader(),
                method: 'POST',
                data: {'id': id, 'number': number},
                success(res) {
                    var resp = res.data;
                    if (resp.code === 200) {
                        that.setData({
                            shopCarNum: resp.data.shopCarNum,
                            hideShopPopup: true,
                        });
                        app.alert({'content': '添加成功'})
                    } else {
                        app.alert({'content': resp.msg});
                        that.setData({
                            hideShopPopup: true
                        })
                    }
                }
            });
        }
    },
    buyNow: function () {
        var that = this;
        var data = {
            'type': '',
            'goods': [
                {
                    'id': that.data.info.id,
                    'price': that.data.info.price,
                    'number': that.data.buyNumber
                }
            ]
        };
        wx.navigateTo({
            url: "/pages/order/index?data=" + JSON.stringify(data)
        });
        that.setData({
            hideShopPopup: true,
        })
    },
    /**
     * 规格选择弹出框
     */
    bindGuiGeTap: function () {
        this.setData({
            hideShopPopup: false
        })
    },
    /**
     * 规格选择弹出框隐藏
     */
    closePopupTap: function () {
        this.setData({
            hideShopPopup: true
        })
    },
    numJianTap: function () {
        if (this.data.buyNumber <= this.data.buyNumMin) {
            return;
        }
        var currentNum = this.data.buyNumber;
        currentNum--;
        this.setData({
            buyNumber: currentNum
        });
    },
    numJiaTap: function () {
        if (this.data.buyNumber >= this.data.buyNumMax) {
            return;
        }
        var currentNum = this.data.buyNumber;
        currentNum++;
        this.setData({
            buyNumber: currentNum
        });
    },
    //事件处理函数
    swiperchange: function (e) {
        this.setData({
            swiperCurrent: e.detail.current
        })
    },
    //显示商品的详细信息
    getFoodInfo: function () {
        var that = this;
        wx.request({
            url: app.buildUrl('/food/info'),
            header: app.getRequestHeader(),
            data: {'id': that.data.id},
            success(res) {
                var resp = res.data;
                if (resp.code === 200) {
                    that.setData({
                        info: resp.data.info,
                        shopCarNum: resp.data.shopCarNum,
                        buyNumMax: resp.data.info.stock,
                    });
                } else {
                    app.alert({'content': resp.msg})
                }
                WxParse.wxParse('article', 'html', that.data.info.summary, that, 0);
            }
        });
    },
    // 分享
    onShareAppMessage: function () {
        var that = this;
        const promise = new Promise(resolve => {
            wx.request({
                url: app.buildUrl('/member/share'),
                header: app.getRequestHeader(),
                method: 'POST',
                data: {
                    'url': utils.getCurrentPageUrlWithArgs()
                },
                success(res) {
                    var resp = res.data;
                    if (resp.code === 200) {

                    } else {
                        app.alert({'content': resp.msg})
                    }
                }
            });
            resolve({
                title: that.data.info.name,
                path: '/page/food/info?id=' + that.data.info.id,
            })
        });
        return {
            promise
        }
    }
});
