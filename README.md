# 微信小程序与后台订餐管理系统

#### 介绍

- ​外卖是人们如今生活工作中不可缺少的一部分，基于微信开发订餐小程序，让订餐变的更加便捷。本项目为个人学习项目。通过微信开发者平台提供的api文档完成小程序部分的开发，由Flask完成相应接口的编写，而小程序后台管理系统，则完全由Flask开发完成，后端数据由MySQL进行存储。
- 此项目分为两部分开发，第一部分为微信小程序开发，第二部分为微信小程序后台开发。后台开发现已经全部完成，但小程序部分由于微信支付需要相关工商证明的证件才能开通，所以有关支付部分和支付回调部分及相关的模块并没有完成测试，只是完成了相关代码的编写。

#### 项目细节

1. 小程序通过请求Flask编写的接口，完成了用户授权登入，菜品的展示，搜索，滑动分页，添加购物车，分享，商品下单，微信支付，订阅消息提示，购买评论等基本功能。
1. 小程序下单过程中在数据库并发控制上采用了悲观锁的方式进行处理，支付成功回调消息发送则使用了数据队列，由后台编写相应的job文件进行异步处理。
1. 小程序后台管理系统完成了账户管理，会员管理，美餐管理，财务管理和统计管理模块的开发。授权登入使用了拦截器技术，图片上传由ajax提交，iframe内联框架的方式实现图片 无刷新上传等，提升用户体验。
1. 由统一资源管理器，进行全局路由管理，编写异步Job进行，进行会员，美餐，营业额的全站统计，定时执行，实现每日更新全站相关数据。


#### 软件架构
​	本系统后台开发采用的是**flask**框架。项目结构如下：

```
├── application.py：应用程序全局对象
├── common：公有库以及model数据结构
│ ├── libs：公有库
│ └── models：数据结构
├── config：配置文件
│ ├── base_settings.json：基础配置
│ ├── local_settings.json：本地配置
│ └── production_setting.json： 线上配置
├── docs：数据库变更记录
│ └── Mysql.md
├── jobs：定时任务
│ ├── launcher：定时任务的分发器
│ ├── bin：定时任务的脚本
│ └── tasks：crontab
├── manager.py：启动入口
├── readme.md
├── release.sh：系统自己的脚本，更新，重启等
├── requirements.txt：需要的扩展依赖
├── uwsgi.ini：生产环境的uwsgi
├── web：http相关代码
│ ├── controllers：controller控制层
│ ├── intercetor：拦截器相关
│ ├── static：静态文件
│ └── template：模板文件
└── www.py：http相关初始化，如：注册蓝图，url等
```

小程序端主要参考微信官方的Api文档:https://developers.weixin.qq.com/miniprogram/dev/api/

#### 安装教程

1. 安装相关的依赖包：

   ```
   pip install -r requirements.txt
   ```

2. windows环境下运行

   ```
   先执行:set opt_config=local 或者 set opt_config=production (local为本地开发配置，production为生产开发配置)
   
   在执行:python manage.py runserver
   ```

3. Linux环境下运行

   ```
   export opt_config=local && python manage.py runserver (根据需要自行切换环境)
   ```

4. 数据库

   ```
   本项目所用相关的sql文件保存在项目的docs文件夹下
   
   第一步：pip install flask-sqlacodegen
   
   第二步：mysql数据库中导入并执行sql文件生成数据库表，然后使用flask-sqlacodegen反向生成model文件
   ```

   **flask-sqlacodegen 使用说明：**

   ```
   根据数据库名迁移:
      flask-sqlacodegen "mysql://root:pwd@127.0.0.1/db_name" --outfile "common/models/Model.py"  --flask
   根据数据库表名迁移:
      flask-sqlacodegen mysql://root:pwd@127.0.0.1/db_name --tables user --outfile "models/Model.py" --flask
   ```

#### 异步Job执行

​	本项目异步Job的编写主要有两部分，分为小程序回调的订阅消息处理和统计处理，统计分为每日会员统计、每日美餐统计和每日全站统计：

自定义执行命令介绍：

```
消息队列处理
	python manager.py runjob -m queue/index
```

```
会员统计：
	python manage.py runjob -m statistics/daily -a member -p yyyy-mm-dd
	
美餐统计：
	python manage.py runjob -m statistics/daily -a food -p yyyy-mm-dd
	
全站统计：
	python manage.py runjob -m statistics/daily -a site -p yyyy-mm-dd
```

#### 项目展示

1. **后台部分展示**

   注：仪表盘为Job模拟数据

   <div align=center><img  src="https://gitee.com/liu928011/mini-program/raw/master/web/static/images/common/image-20210805194227457.png"/></div>

   <div align=center><img  src="https://gitee.com/liu928011/mini-program/raw/master/web/static/images/common/image-20210805194325227.png"/></div>

   <div align=center><img  src="https://gitee.com/liu928011/mini-program/raw/master/web/static/images/common/image-20210805194412048.png"/></div>

   <div align=center><img  src="https://gitee.com/liu928011/mini-program/raw/master/web/static/images/common/image-20210805194436438.png"/></div>

   <div align=center><img  src="https://gitee.com/liu928011/mini-program/raw/master/web/static/images/common/image-20210805194521309.png"/></div>

   <div align=center><img  src="https://gitee.com/liu928011/mini-program/raw/master/web/static/images/common/image-20210805194612896.png"/></div>

   

2. **小程序部分展示**

   <div align=center><img  src="https://gitee.com/liu928011/mini-program/raw/master/web/static/images/common/image-20210805195154779.png"/></div>
   
   
   
   <div align=center><img  src="https://gitee.com/liu928011/mini-program/raw/master/web/static/images/common/image-20210805195313579.png"/></div>



<div align=center><img  src="https://gitee.com/liu928011/mini-program/raw/master/web/static/images/common/image-20210805195559720.png"/></div>



<div align=center><img  src="https://gitee.com/liu928011/mini-program/raw/master/web/static/images/common/image-20210805195417251.png"/></div>



<div align=center><img  src="https://gitee.com/liu928011/mini-program/raw/master/web/static/images/common/image-20210805195456090.png"/></div>



