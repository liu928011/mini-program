from common.models.statistics import StatDailySite
from common.models.statistics import StatDailyFood
from common.models.statistics import StatDailyMember
from common.libs.Helper import getDictFilterFiled
from common.libs.Helper import selectFilterObj
from common.libs.Helper import ops_render
from common.models.food import Food
from common.models.member import Member
from flask import Blueprint
from flask import request
from application import app

route_statistics = Blueprint('statistics_page', __name__)

@route_statistics.route('/index')
def index():
    resp_data = {}
    resp = request.values
    query = StatDailySite.query
    date_from = resp.get('date_from')
    date_to = resp.get('date_to')

    if 'date_from' in resp and date_from:
        query = query.filter(StatDailySite.date >= date_from)

    if 'date_to' in resp and date_to:
        query = query.filter(StatDailySite.date <= date_to)

    # 指定当前页码
    page = resp.get('page', type=int)
    # 定义每页显示数
    page_size = app.config['PAGE_SIZE']
    page_url = request.full_path.replace('&page={}'.format(page), '')
    # 创建分页对象
    pagination = query.order_by(StatDailySite.id.asc()).paginate(page, page_size, False)

    resp_data['current'] = 'index'
    resp_data['page_url'] = page_url
    resp_data['pagination'] = pagination
    resp_data['search'] = resp
    return ops_render('stat/index.html', resp_data)

@route_statistics.route('/member')
def member():
    resp_data = {}
    resp = request.values
    query = StatDailyMember.query
    date_from = resp.get('date_from')
    date_to = resp.get('date_to')

    if 'date_from' in resp and date_from:
        query = query.filter(StatDailyMember.date >= date_from)

    if 'date_to' in resp and date_to:
        query = query.filter(StatDailyMember.date <= date_to)

    # 指定当前页码
    page = resp.get('page', type=int)
    # 定义每页显示数
    page_size = app.config['PAGE_SIZE']
    page_url = request.full_path.replace('&page={}'.format(page), '')
    # 创建分页对象
    pagination = query.order_by(StatDailyMember.id.asc()).paginate(page, page_size, False)

    member_items_list = StatDailyMember.query.all()
    member_ids = selectFilterObj(member_items_list, 'member_id')
    Member_mapping = getDictFilterFiled(Member, Member.id, 'id', member_ids)

    resp_data['current'] = 'member'
    resp_data['search'] = resp
    resp_data['page_url'] = page_url
    resp_data['pagination'] = pagination
    resp_data['Member_mapping'] = Member_mapping

    return ops_render('stat/member.html', resp_data)

@route_statistics.route('/share')
def share():
    resp_data = {}
    resp = request.values
    query = StatDailySite.query
    date_from = resp.get('date_from')
    date_to = resp.get('date_to')

    if 'date_from' in resp and date_from:
        query = query.filter(StatDailySite.date >= date_from)

    if 'date_to' in resp and date_to:
        query = query.filter(StatDailySite.date <= date_to)

    # 指定当前页码
    page = resp.get('page', type=int)
    # 定义每页显示数
    page_size = app.config['PAGE_SIZE']
    page_url = request.full_path.replace('&page={}'.format(page), '')
    # 创建分页对象
    pagination = query.order_by(StatDailySite.id.asc()).paginate(page, page_size, False)

    resp_data['current'] = 'share'
    resp_data['page_url'] = page_url
    resp_data['pagination'] = pagination
    resp_data['search'] = resp

    return ops_render('stat/share.html',resp_data)

@route_statistics.route('/food')
def food():
    resp_data = {}
    resp = request.values
    query = StatDailyFood.query
    date_from = resp.get('date_from')
    date_to = resp.get('date_to')

    if 'date_from' in resp and date_from:
        query = query.filter(StatDailyFood.date >= date_from)

    if 'date_to' in resp and date_to:
        query = query.filter(StatDailyFood.date <= date_to)

    # 指定当前页码
    page = resp.get('page', type=int)
    # 定义每页显示数
    page_size = app.config['PAGE_SIZE']
    page_url = request.full_path.replace('&page={}'.format(page), '')
    # 创建分页对象
    pagination = query.order_by(StatDailyFood.id.asc()).paginate(page, page_size, False)

    food_items_list = StatDailyFood.query.all()
    food_ids = selectFilterObj(food_items_list, 'food_id')
    food_mapping = getDictFilterFiled(Food, Food.id, 'id', food_ids)

    resp_data['current'] = 'food'
    resp_data['search'] = resp
    resp_data['page_url'] = page_url
    resp_data['pagination'] = pagination
    resp_data['food_mapping'] = food_mapping

    return ops_render('stat/food.html', resp_data)
