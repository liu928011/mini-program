from flask import Blueprint
from flask import jsonify
from common.models.statistics import StatDailySite
from common.libs.Helper import getFormatDate
import datetime

route_chart = Blueprint('chart_page', __name__)

@route_chart.route('/dashborad')
def dashboard():
    """
    仪表盘
    :return:
    """
    result = {'code': 200, 'data': {}}

    now_date = datetime.datetime.now()
    before_date = now_date - datetime.timedelta(days=30)
    date_from = getFormatDate(before_date, format='%Y-%m-%d')
    date_to = getFormatDate(now_date, format='%Y-%m-%d')

    # 定义数据返回格式
    data = {
        'text': '会员与订单统计',
        'categories': [],
        'series': [
            {
                'name': '会员总数',
                'data': []
            },
            {
                'name': '订单总数',
                'data': []
            }
        ]
    }
    stat_site_item_list = StatDailySite.query.filter(StatDailySite.date >= date_from, StatDailySite.date <= date_to) \
        .order_by(StatDailySite.id.asc()).all()
    if stat_site_item_list:
        for item in stat_site_item_list:
           data['categories'].append(getFormatDate(item.date, format='%Y-%m-%d'))
           data['series'][0]['data'].append(item.total_member_count)
           data['series'][1]['data'].append(item.total_order_count)

    result['data']=data

    return jsonify(result)

@route_chart.route('/finance')
def finance():
    result = {'code': 200, 'data': {}}

    now_date = datetime.datetime.now()
    before_date = now_date - datetime.timedelta(days=30)
    date_from = getFormatDate(before_date, format='%Y-%m-%d')
    date_to = getFormatDate(now_date, format='%Y-%m-%d')

    # 定义数据返回格式
    data = {
        'text':'日营收报表',
        'categories': [],
        'series': [
            {
                'name': '日营收报表',
                'data': []
            },
        ]
    }
    stat_site_item_list = StatDailySite.query.filter(StatDailySite.date >= date_from, StatDailySite.date <= date_to) \
        .order_by(StatDailySite.id.asc()).all()

    if stat_site_item_list:
        for item in stat_site_item_list:
           data['categories'].append(getFormatDate(item.date, format='%Y-%m-%d'))
           data['series'][0]['data'].append(float(item.total_pay_money))

    result['data']=data

    return jsonify(result)

@route_chart.route('/share')
def share():
    result = {'code': 200, 'data': {}}

    now_date = datetime.datetime.now()
    before_date = now_date - datetime.timedelta(days=30)
    date_from = getFormatDate(before_date, format='%Y-%m-%d')
    date_to = getFormatDate(now_date, format='%Y-%m-%d')

    # 定义数据返回格式
    data = {
        'text':'日分享情况统计',
        'categories': [],
        'series': [
            {
                'name': '日分享总数',
                'data': []
            },
        ]
    }
    stat_site_item_list = StatDailySite.query.filter(StatDailySite.date >= date_from, StatDailySite.date <= date_to) \
        .order_by(StatDailySite.id.asc()).all()

    if stat_site_item_list:
        for item in stat_site_item_list:
           data['categories'].append(getFormatDate(item.date, format='%Y-%m-%d'))
           data['series'][0]['data'].append(float(item.total_shared_count))

    result['data']=data

    return jsonify(result)