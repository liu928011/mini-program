from web.controllers.api import route_api
from flask import request, jsonify,g
from common.libs.member.MemberService import MemberService
from common.models.member import Member
from common.models.member import OauthMemberBind
from common.models.food import WxShareHistory
from common.libs.Helper import getCurrentDate


@route_api.route('/member/login', methods=['POST'])
def login():
    result = {'code': 200, 'msg': '登录成功', 'data': {}}
    data = request.get_json()
    code = data.get('code')
    nickname = data.get('nickName')
    sex = data.get('gender')
    avatar = data.get('avatarUrl')

    if not code:
        result['code'] = -1
        result['msg'] = '授权失败!'
        return jsonify(result)

    # 获取用户的唯一标识openid
    openid = MemberService.getWetChatOpenId(code)

    if not openid:
        result['code'] = -1
        result['msg'] = '授权失败！'
        return jsonify(result)

    # 根据用户的openid判断用户有没有注册过
    member_bind = OauthMemberBind.query.filter_by(openid=openid).first()
    if not member_bind:
        member = Member()
        member.nickname = nickname
        member.sex = sex
        member.avatar = avatar
        member.salt = MemberService.geneSalt()
        member.updated_time = getCurrentDate()
        member.created_time = getCurrentDate()
        member.save()

        member_bind = OauthMemberBind()
        member_bind.member_id = member.id
        member_bind.openid = openid
        member_bind.type = 1
        member_bind.extra = ''
        member_bind.updated_time = getCurrentDate()
        member_bind.created_time = getCurrentDate()
        member_bind.save()

    member_id = member_bind.member_id
    member = Member.query.filter_by(id=member_id).first()
    token = '{}#{}'.format(MemberService.geneAuthCode(member), member.id)
    result['data']={'token':token}
    return jsonify(result)

@route_api.route('/member/check-register', methods=['POST'])
def checkReg():
    result = {'code': 200, 'msg': '已授权', 'data': {}}
    data = request.get_json()
    code = data.get('code')

    # 获取用户的唯一标识openid
    openid = MemberService.getWetChatOpenId(code)
    if not code:
        result['code'] = -1
        result['msg'] = 'Login error!'
        return jsonify(result)

    if not openid:
        result['code'] = -1
        result['msg'] = 'openid error'
        return jsonify(result)

    member_bind = OauthMemberBind.query.filter_by(openid=openid).first()
    if not member_bind:
        result['code'] = -1
        result['msg'] = '用户未绑定'
        return jsonify(result)

    member_id = member_bind.member_id
    member = Member.query.filter_by(id=member_id).first()
    if not member:
        result['code'] = -1
        result['msg'] = '未查询到绑定信息'
        return jsonify(result)

    token = '{}#{}'.format(MemberService.geneAuthCode(member),member.id)
    result['data'] = {'token': token}
    return jsonify(result)

@route_api.route('/member/share',methods=['POST'])
def MemberShare():
    result = {'code': 200, 'msg': '操作成功!', 'data': {}}
    resp = request.values
    url = resp.get('url')
    if not url:
        result['code']=-1
        result['msg']='分享失败！'
        return jsonify(result)
    share = WxShareHistory()
    if g.current_member:
        share.member_id = g.current_member.id
    else:
        result['code'] = -1
        result['msg'] = '分享失败,请先登入！'
        return jsonify(result)
    share.share_url = url
    share.created_time = getCurrentDate()
    share.save()
    return jsonify(result)




