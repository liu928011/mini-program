from application import app
from common.libs.Logs import UserLogs
from common.libs.Helper import ops_render

# 错误处理
@app.errorhandler(404)
def error_404(error):
    UserLogs.error_log(error)
    return ops_render('error/error.html',{'msg':'很抱歉，你访问的页面不存在！'})



