;
var food_cat_set_ops = {
    init: function () {
        this.eventBind()
    },
    eventBind: function () {
        $('.save').click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass('disabled')) {
                common_ops.alert('正在处理中，请不要重复提交');
                return false
            }
            cat_name_target = $('.wrap_cat_set input[name=cat_name]');
            cat_name = cat_name_target.val();

            weight_target = $('.wrap_cat_set input[name=weight]');
            weight = weight_target.val();

            id = $('.wrap_cat_set input[name=id]').val();

            if (!cat_name || cat_name.length < 1) {
                common_ops.tip('分类名不能为空', cat_name_target);
                return false
            }
            var regCatName = /^[\u4e00-\u9fa5_a-zA-Z0-9]+$/;
            if (!regCatName.test(cat_name)) {
                common_ops.tip('分类名不能包含特殊字符', cat_name_target);
                return false
            }
            var regWeight = /^[0-9]+$/;
            if (!regWeight.test(weight)) {
                common_ops.tip('请输入权重如：1、2、3....', weight_target);
                return false
            }
            btn_target.addClass('disabled');
            $.ajax({
                url: common_ops.buildUrl('/food/cat-set'),
                type: 'POST',
                dataType: 'json',
                data: {'id': id, 'cat_name': cat_name, 'weight': weight},
                success: function (res) {
                    btn_target.removeClass('disabled');
                    var callback = null;
                    if (res.code === 200) {
                        callback = function () {
                            window.location.href = common_ops.buildUrl('/food/cat')
                        };
                        common_ops.alert(res.msg, callback)
                    }
                    if (res.code === -1) {
                        common_ops.alert(res.msg);
                        return false
                    }
                },
                error:function (err) {
                    console.log(err)
                }
            })
        });

    }
};

$(document).ready(function () {
    food_cat_set_ops.init()
});