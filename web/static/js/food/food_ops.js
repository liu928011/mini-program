;
var ops = {
    init: function () {
        this.eventBind()
    },
    eventBind: function () {
        var that = this;
        $('.wrap_search .search').click(function () {
            $('.wrap_search').submit()
        });
        $('.remove').click(function () {
            that.ops('remove', $(this).attr('data'))
        });
        $('.recover').click(function () {
            that.ops('recover', $(this).attr('data'))
        })
    },
    ops: function (act, id) {
        var callback = {
            ok: function () {
                $.ajax({
                    url: common_ops.buildUrl('/food/food-ops'),
                    type: 'POST',
                    data: {'id': id, 'action': act},
                    dataType: 'json',
                    success: function (res) {
                        if (res.code === 200) {
                            window.location.reload()
                        }
                        if (res.code === -1) {
                            common_ops.alert(res.msg)
                        }
                    }
                })
            },
            cancel: null
        };
        common_ops.confirm(act === 'remove' ? '确定删除？' : '确定恢复？', callback)

    }
};
$(document).ready(function () {
    ops.init()
});