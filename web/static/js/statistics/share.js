;
var statistics_share_ops = {
    init: function () {
        this.eventBind();
        this.drawChart();
        this.dateTimePickerComponent()
    },
    eventBind:function(){
        $('#search_form_wrap .search').click(function () {
            $('#search_form_wrap').submit()
        })
    },
    dateTimePickerComponent: function () {
        $.datetimepicker.setLocale('zh');//使用中文
        var params = {
            scrollInput: false,
            scrollMonth: false,
            scrollTime: false,
            dayOfWeekStart: 1,
            todayButton: true,//回到今天
            defaultDate: new Date(),
            defaultSelect: true,
            timepicker: false,
            format: 'Y-m-d'
        };
        $('#search_form_wrap input[name=date_from]').datetimepicker(params);
        $('#search_form_wrap input[name=date_to]').datetimepicker(params)
    },

    drawChart: function () {
        charts_ops.setOption();
        $.ajax({
            url: common_ops.buildUrl('/chart/share'),
            dataType: 'json',
            success: function (res) {
                charts_ops.drawLine($('#container'), res.data)
            }
        })
    }
};

$(document).ready(function () {
    statistics_share_ops.init()
});