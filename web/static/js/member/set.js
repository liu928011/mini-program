;
var member_set_ops = {
    init: function () {
        this.eventBind()
    },
    eventBind: function () {
        $('.save').click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass('disabled')) {
                common_ops.alert('正在处理中，请不要重复提交');
                return false
            }
            id = $('.wrap_member_set input[name=id]').val();
            nickname_target = $('.wrap_member_set input[name=nickname]');
            nickname = nickname_target.val();

            var regNickname = /^[\u4e00-\u9fa5_a-zA-Z0-9]+$/;
            if (!regNickname.test(nickname)) {
                common_ops.tip('昵称不能包含特殊字符', nickname_target);
                return false
            }
            btn_target.addClass('disabled');
            $.ajax({
                url: common_ops.buildUrl('/member/set'),
                type: 'POST',
                data: {'id': id, 'nickname': nickname},
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass('disabled');
                    var callback = null;
                    if (res.code === 200) {
                        callback = function () {
                            window.location.href = common_ops.buildUrl('/member/index')
                        };
                        common_ops.alert(res.msg, callback)
                    }
                    if (res.code === -1) {
                        common_ops.alert(res.msg);
                        return false
                    }
                },
                error: function (res) {
                    console.log(res)
                }
            })

        })
    }
};

$(document).ready(function () {
    member_set_ops.init()
});