;

var finance_info_ops = {
    init: function () {
        this.eventBind()
    },
    eventBind: function () {
        $('.wrap_info .address_send').click(function () {
            var id = $(this).attr('data');
            var param = {
                ok: function () {
                    $.ajax({
                        url: common_ops.buildUrl('/finance/ops'),
                        method: 'POST',
                        data: {'id': id},
                        dataType: 'json',
                        success: function (res) {
                            if (res.code === 200) {
                                // 刷新
                                window.location.reload();
                            } else {
                                common_ops.alert(res.msg)
                            }
                        }
                    })
                },
                cancel: null
            };
            common_ops.confirm('确认发货？', param)
        })
    }
};

$(document).ready(function () {
    finance_info_ops.init()
});