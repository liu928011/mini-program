;
var user_mod_pwd_ops = {
    init: function () {
        this.eventBind()
    },
    eventBind: function () {
        $('#save').click(function () {
            //this操作的是HTML对象,$(this)是jQuery对象，且只在该函数有效
            var btn_target = $(this);
            if (btn_target.hasClass('disabled')) {
                common_ops.alert('正在处理中，请不要重复提交');
                return false
            }
            var old_password_target = $('#old_password');
            var old_password = old_password_target.val();
            var new_password_target = $('#new_password');
            var new_password = new_password_target.val();

            // 前端校验
            if (!old_password) {
                common_ops.tip('原密码不能为空', old_password_target);
                return false
            }
            if (!new_password) {
                common_ops.tip('请输入新密码', new_password_target);
                return false
            }
            if (new_password.length < 6) {
                common_ops.tip('密码长度不能小于6位', new_password_target);
                return false
            }
            btn_target.addClass('disabled');
            var data = {
                'old_password': old_password,
                'new_password': new_password
            };
            $.ajax({
                url: common_ops.buildUrl('/user/reset_pwd'),
                data: data,
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass('disabled');
                    var callback = null;
                    if (res.code === 200) {
                        callback = function () {
                            window.location.reload();
                        };
                        common_ops.alert(res.msg, callback)
                    }
                    if (res.code === -1) {
                        common_ops.alert(res.msg)
                    }
                }
            })
        });

    }
};

$(document).ready(function () {
    user_mod_pwd_ops.init()
});