;
var user_login_ops = {
    init: function () {
        this.eventBind() //初始化执行绑定事件
    },
    eventBind: function () {
        $('.login-wrap .login-btn').click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass('disabled')) {
                common_ops.alert('正在处理中，请不要重复提交！');
                return false;
            }
            var login_name = $('.login-wrap input[ name=login_name]').val();
            var login_pwd = $('.login-wrap input[ name=login_pwd]').val();

            btn_target.addClass('disabled'); //第一次点击后按钮变灰
            $.ajax({
                url: common_ops.buildUrl('/user/login'),
                type: 'post',
                data: {'login_name': login_name, 'login_pwd': login_pwd},
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass('disabled'); //执行成功后，移除disabled
                    // var callback=null;
                    if (res.code === 200) {
                        window.location.href = common_ops.buildUrl("/");
                        // callback =function(){
                        //     window.location.href = common_ops.buildUrl("/");
                        // };
                        // common_ops.alert(res.msg,callback)
                    }
                    if (res.code === -1) {
                        common_ops.alert(res.msg);
                    }
                },
            })
        })
    }
};
$(document).ready(function () {
        user_login_ops.init() //等页面加载完成执行初始化
    }
);