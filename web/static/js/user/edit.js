;
var user_edit_ops = {
    init: function () {
        this.eventBind()
    },
    eventBind: function () {
        $('.user_edit_wrap .save').click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass('disabled')) {
                common_ops.alert('正在处理中，请不要重复提交！');
                return false
            }
            var nickname_target = $('.user_edit_wrap input[name=nickname]');
            var nickname = nickname_target.val();
            var email_target = $('.user_edit_wrap input[name=email]');
            var email = email_target.val();

            //前端校验
            if (!nickname || nickname.length < 1) {
                common_ops.tip('昵称不能为空！', nickname_target);
                return false
            }
            // 校验邮箱的正则
            var reg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
            if (!reg.test(email)) {
                common_ops.tip('请输入正确的邮箱格式', email_target);
                return false
            }
            btn_target.addClass('disabled');
            var data = {
                'nickname': nickname,
                'email': email
            };
            $.ajax({
                url: common_ops.buildUrl('/user/edit'),
                type: 'post',
                data: data,
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass('disabled');
                    var callback = null;
                    if (res.code === 200) {
                        callback = function () {
                            window.location.reload(); //刷新当前页面
                        };
                        common_ops.alert(res.msg, callback)
                    }
                    if(res.code===-1){
                        common_ops.alert(res.msg)
                    }

                },

            })

        })

    }
};

$(document).ready(function () {
    user_edit_ops.init()
});