;
var account_set_ops = {
    init: function () {
        this.eventBind()
    },
    eventBind: function () {
        $('.wrap_account_set .save').click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass('disabled')) {
                common_ops.alert('正在处理中，请不要重复提交');
                return false
            }
            nickname_target = $('.wrap_account_set input[name=nickname]');
            nickname = nickname_target.val();

            mobile_target = $('.wrap_account_set input[name=mobile]');
            mobile = mobile_target.val();

            email_target = $('.wrap_account_set input[name=email]');
            email = email_target.val();

            login_name_target = $('.wrap_account_set input[name=login_name]');
            login_name = login_name_target.val();

            login_pwd_target = $('.wrap_account_set input[name=login_pwd]');
            login_pwd = login_pwd_target.val();

            // 昵称校验
            if (!nickname || nickname.length < 1) {
                common_ops.tip('昵称不能为空！', nickname_target);
                return false
            }

            // 校验手机号的正则
            var regPhone = /^1[3456789]{1}\d{9}$/;
            if (!regPhone.test(mobile)) {
                common_ops.tip('请输入正确的手机号格式', mobile_target);
                return false
            }

            // 校验邮箱的正则
            var regEmail = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
            if (!regEmail.test(email)) {
                common_ops.tip('请输入正确的邮箱格式', email_target);
                return false
            }

            //用户名校验正则
            var regName = /^[a-zA-Z]\w{3,15}$/;
            if (!regName.test(login_name)) {
                common_ops.tip('4-16位字母、数字或下划线，以字母开头', login_name_target);
                return false
            }
            // console.log(login_pwd.length);
            //密码校验
            if (!login_pwd||login_pwd.length < 6 || login_pwd.length > 32) {
                common_ops.tip('密码长度不能小于6位，大于32位', login_pwd_target);
                return false
            }
            btn_target.addClass('disabled');
            var id = $('.wrap_account_set input[name=id]').val();
            var data = {
                'id': id,
                'nickname': nickname,
                'mobile': mobile,
                'email': email,
                'login_name': login_name,
                'login_pwd': login_pwd
            };
            $.ajax({
                url: common_ops.buildUrl('/account/set'),
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (res) {
                    btn_target.removeClass('disabled');
                    var callback = null;
                    if (res.code === 200) {
                        callback = function () {
                            window.location.href = common_ops.buildUrl('/account/index')
                        };
                        common_ops.alert(res.msg, callback)
                    }
                    if (res.code === -1) {
                        common_ops.alert(res.msg)
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })
        })
    }
};

$(document).ready(function () {
    account_set_ops.init()
});