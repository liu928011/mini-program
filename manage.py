from  application import app,manager
from flask_script import Server
from jobs.launcher import RunJob
import traceback
import www

manager.add_command('runserver',Server(host='127.0.0.1',port=app.config['SERVER_PORT']))
manager.add_command('runjob',RunJob())

if __name__ == '__main__':
    try:
        manager.run()
    except Exception as e:
        traceback.print_exc()





