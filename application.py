from config.base_sittings import STATIC_ROOT
from config.base_sittings import TEMPLATES_ROOT
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask import Flask
import os

db = SQLAlchemy()

class Application(Flask):
    """  定义app类  """
    def __init__(self, import_name, template_folder=None, static_folder=None):
        super(Application, self).__init__(import_name, template_folder=template_folder, static_folder=static_folder)
        self.config.from_pyfile('config/base_sittings.py')
        if 'opt_config' in os.environ:  # 配置动态切换
            self.config.from_pyfile('config/%s_sittings.py' % os.environ['opt_config'])

        db.init_app(self)

# app初始化
app = Application(__name__, template_folder=TEMPLATES_ROOT, static_folder=STATIC_ROOT)  # 实例化app
manager = Manager(app)


# 模板函数
# 可以使用app.template_global装饰器直接将函数注册为模板全局函数(就是说模板中也可以使用该函数)。
# 也可以直接使用app.add_template_global()方法注册自定义全局函数，第一个参数是你的方法,第二个参数是你方法的名字
from common.libs.UrlManager import UrlManager

app.add_template_global(UrlManager.buildUrl, 'buildUrl')
app.add_template_global(UrlManager.buildStaticUrl, 'buildStaticUrl')
app.add_template_global(UrlManager.buildImageUrl, 'buildImageUrl')
