import os

BASE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

# 服务器端口号
SERVER_PORT = 5000

# 是否开启调试
DEBUG = False

# 登入用户的cookie的键值
AUTH_COOKIE_NAME = 'cookie'

# 静态文件夹路径
STATIC_ROOT = os.path.join(BASE_DIR, 'web/static')

# 模板文件夹路径
TEMPLATES_ROOT = os.path.join(BASE_DIR, 'web/templates')

APP = {
    'domain': 'http://127.0.0.1:5000'
}

# 忽略拦截的路由
IGNORE_URLS = [
    '^/user/login',
]

IGNORE_CHECK_URLS = [
    '^/static',
    '^/favicon.ico'
]

# api忽略拦截的路由
API_IGNORE_URLS = [
    '^/api'
]
# 分页设置

PAGE_SIZE = 20  # 每页大小
PAGE_DISPLAY = 10  # 页面上共显示10页

STATUS_MAPPING = {
    '1': '正常',
    '0': '已删除'
}

# 小程序测试号的信息设置
MINA_INFO = {
    'appid': 'wx1c28d68af19b6b1e',
    'secret': '941c96695415150d2d2e2578ea087616',
}

# 测试专用
PAY_INFO = {
    'appid': 'wx8397f8696b538317',
    'mch_id': '1473426802',
    'merchant_key': 'T6m9iK73b0kn9g5v426MKfHQH7X8rKwb',
    'callback_url': '/api/order/callback'
}

UPLOAD_IMAGES = {
    'ext': ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
    'file_root': 'web/static/upload-images',
    'image_root': 'static/upload-images'
}

PAY_ORDER_STATUS_MAPPING = {
    '-8': '待付款',
    '-7': '待发货',
    '-6': '待收货',
    '-5': '待评价',
    '1': '已完成',
    '0': '已关闭',
}


# 自动生成订单时配置，分布式部署时，不同主机的配置应该不一致
DEVICE_NUM = 6
