# coding: utf-8
from common.models import BaseModel
from application import db

class User(BaseModel):
    __tablename__ = 'user'

    uid = db.Column(db.BigInteger, primary_key=True, comment='用户id')
    nickname = db.Column(db.String(100), nullable=False, server_default=db.FetchedValue(), comment='昵称')
    mobile = db.Column(db.String(20), nullable=False, server_default=db.FetchedValue(), comment='手机号码')
    email = db.Column(db.String(100), nullable=False, server_default=db.FetchedValue(), comment='邮箱地址')
    sex = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='1：男 2：女 0：没填写')
    avatar = db.Column(db.String(64), nullable=False, server_default=db.FetchedValue(), comment='头像')
    login_name = db.Column(db.String(20), nullable=False, unique=True, server_default=db.FetchedValue(), comment='登录用户名')
    login_pwd = db.Column(db.String(32), nullable=False, server_default=db.FetchedValue(), comment='登录密码')
    login_salt = db.Column(db.String(32), nullable=False, server_default=db.FetchedValue(), comment='登录密码的随机加密秘钥')
    status = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='1：有效 0：无效')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后一次更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')
