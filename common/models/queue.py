from common.models import BaseModel
from application import db



class QueueList(BaseModel):
    __tablename__ = 'queue_list'

    id = db.Column(db.Integer, primary_key=True)
    queue_name = db.Column(db.String(30), nullable=False, server_default=db.FetchedValue(), comment='队列名字')
    data = db.Column(db.String(500), nullable=False, server_default=db.FetchedValue(), comment='队列数据')
    status = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='状态 -1 待处理 1 已处理')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后一次更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')


class OauthAccessToken(BaseModel):
    __tablename__ = 'oauth_access_token'

    id = db.Column(db.Integer, primary_key=True)
    access_token = db.Column(db.String(600), nullable=False, server_default=db.FetchedValue())
    expired_time = db.Column(db.DateTime, nullable=False, index=True, server_default=db.FetchedValue(), comment='过期时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')