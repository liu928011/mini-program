# coding: utf-8
from common.models import BaseModel
from application import db
from application import app


class Member(BaseModel):
    __tablename__ = 'member'

    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(100), nullable=False, server_default=db.FetchedValue(), comment='会员名')
    mobile = db.Column(db.String(11), nullable=False, server_default=db.FetchedValue(), comment='会员手机号码')
    sex = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='性别 0:未知 1：男 2：女')
    avatar = db.Column(db.String(200), nullable=False, server_default=db.FetchedValue(), comment='会员头像')
    salt = db.Column(db.String(32), nullable=False, server_default=db.FetchedValue(), comment='随机salt')
    reg_ip = db.Column(db.String(100), nullable=False, server_default=db.FetchedValue(), comment='注册的ip')
    status = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='状态 1：正常 0：无效')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后一次更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')

    @property
    def status_desc(self):
        return app.config['STATUS_MAPPING'][str(self.status)]

    @property
    def sex_desc(self):
        sex_mapping = {'0':'未知', '1':'男','2':'女'}
        return sex_mapping[str(self.sex)]


class OauthMemberBind(BaseModel):
    __tablename__ = 'oauth_member_bind'
    __table_args__ = (
        db.Index('idx_type_openid', 'type', 'openid'),
    )
    id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='会员id')
    client_type = db.Column(db.String(20), nullable=False, server_default=db.FetchedValue(), comment='客户端来源类型。qq,weibo,weixin')
    type = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='类型 type 1:wechat ')
    openid = db.Column(db.String(80), nullable=False, server_default=db.FetchedValue(), comment='第三方id')
    unionid = db.Column(db.String(100), nullable=False, server_default=db.FetchedValue())
    extra = db.Column(db.Text, nullable=False, comment='额外字段')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')


class MemberComment(BaseModel):
    __tablename__ = 'member_comments'

    id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.Integer, nullable=False, index=True, server_default=db.FetchedValue(), comment='会员id')
    food_ids = db.Column(db.String(200), nullable=False, server_default=db.FetchedValue(), comment='商品ids')
    pay_order_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='订单id')
    score = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='评分')
    content = db.Column(db.String(200), nullable=False, server_default=db.FetchedValue(), comment='评论内容')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')

class MemberAddress(BaseModel):
    __tablename__ = 'member_address'
    __table_args__ = (
        db.Index('idx_member_id_status', 'member_id', 'status'),
    )

    id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='会员id')
    nickname = db.Column(db.String(20), nullable=False, server_default=db.FetchedValue(), comment='收货人姓名')
    mobile = db.Column(db.String(11), nullable=False, server_default=db.FetchedValue(), comment='收货人手机号码')
    province_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='省id')
    province_str = db.Column(db.String(50), nullable=False, server_default=db.FetchedValue(), comment='省名称')
    city_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='城市id')
    city_str = db.Column(db.String(50), nullable=False, server_default=db.FetchedValue(), comment='市名称')
    area_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='区域id')
    area_str = db.Column(db.String(50), nullable=False, server_default=db.FetchedValue(), comment='区域名称')
    address = db.Column(db.String(100), nullable=False, server_default=db.FetchedValue(), comment='详细地址')
    status = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='是否有效 1：有效 0：无效')
    is_default = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='默认地址')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后一次更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')