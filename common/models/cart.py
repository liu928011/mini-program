from common.models import BaseModel
from application import db

class MemberCart(BaseModel):
    __tablename__ = 'member_cart'

    id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.BigInteger, nullable=False, index=True, server_default=db.FetchedValue(), comment='会员id')
    food_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='商品id')
    quantity = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='数量')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后一次更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')