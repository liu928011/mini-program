# coding: utf-8
from common.models import BaseModel
from application import db

class AppAccessLog(BaseModel):
    __tablename__ = 'app_access_log'

    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.BigInteger, nullable=False, index=True, server_default=db.FetchedValue(), comment='uid')
    referer_url = db.Column(db.String(255), nullable=False, server_default=db.FetchedValue(), comment='当前访问的refer')
    target_url = db.Column(db.String(255), nullable=False, server_default=db.FetchedValue(), comment='访问的url')
    query_params = db.Column(db.Text, nullable=False, comment='get和post参数')
    ua = db.Column(db.String(255), nullable=False, server_default=db.FetchedValue(), comment='访问ua')
    ip = db.Column(db.String(32), nullable=False, server_default=db.FetchedValue(), comment='访问ip')
    note = db.Column(db.String(1000), nullable=False, server_default=db.FetchedValue(), comment='json格式备注字段')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())

class AppErrorLog(BaseModel):
    __tablename__ = 'app_error_log'

    id = db.Column(db.Integer, primary_key=True)
    referer_url = db.Column(db.String(255), nullable=False, server_default=db.FetchedValue(), comment='当前访问的refer')
    target_url = db.Column(db.String(255), nullable=False, server_default=db.FetchedValue(), comment='访问的url')
    query_params = db.Column(db.Text, nullable=False, comment='get和post参数')
    content = db.Column(db.String, nullable=False, comment='日志内容')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')
