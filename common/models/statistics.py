from common.models import BaseModel
from application import db

class StatDailyFood(BaseModel):
    __tablename__ = 'stat_daily_food'
    __table_args__ = (
        db.Index('date_food_id', 'date', 'food_id'),
    )

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, nullable=False)
    food_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='菜品id')
    total_count = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='售卖总数量')
    total_pay_money = db.Column(db.Numeric(10, 2), nullable=False, server_default=db.FetchedValue(), comment='总售卖金额')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后一次更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')

class StatDailyMember(BaseModel):
    __tablename__ = 'stat_daily_member'
    __table_args__ = (
        db.Index('idx_date_member_id', 'date', 'member_id'),
    )

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, nullable=False, comment='日期')
    member_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='会员id')
    total_shared_count = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='当日分享总次数')
    total_pay_money = db.Column(db.Numeric(10, 2), nullable=False, server_default=db.FetchedValue(), comment='当日付款总金额')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后一次更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')

class StatDailySite(BaseModel):
    __tablename__ = 'stat_daily_site'

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, nullable=False, index=True, comment='日期')
    total_pay_money = db.Column(db.Numeric(10, 2), nullable=False, server_default=db.FetchedValue(), comment='当日应收总金额')
    total_member_count = db.Column(db.Integer, nullable=False, comment='会员总数')
    total_new_member_count = db.Column(db.Integer, nullable=False, comment='当日新增会员数')
    total_order_count = db.Column(db.Integer, nullable=False, comment='当日订单数')
    total_shared_count = db.Column(db.Integer, nullable=False)
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后一次更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')
