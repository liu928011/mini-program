from common.models import BaseModel
from application import db
from application import app

class PayOrder(BaseModel):
    __tablename__ = 'pay_order'
    __table_args__ = (
        db.Index('idx_member_id_status', 'member_id', 'status'),
    )

    id = db.Column(db.Integer, primary_key=True)
    order_sn = db.Column(db.String(40), nullable=False, unique=True, server_default=db.FetchedValue(), comment='订单号')
    member_id = db.Column(db.BigInteger, nullable=False, server_default=db.FetchedValue(), comment='会员id')
    total_price = db.Column(db.Numeric(10, 2), nullable=False, server_default=db.FetchedValue(), comment='订单应付金额')
    yun_price = db.Column(db.Numeric(10, 2), nullable=False, server_default=db.FetchedValue(), comment='运费金额')
    pay_price = db.Column(db.Numeric(10, 2), nullable=False, server_default=db.FetchedValue(), comment='订单实付金额')
    pay_sn = db.Column(db.String(128), nullable=False, server_default=db.FetchedValue(), comment='第三方流水号')
    prepay_id = db.Column(db.String(128), nullable=False, server_default=db.FetchedValue(), comment='第三方预付id')
    note = db.Column(db.Text, nullable=False, comment='备注信息')
    status = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(),
                       comment=' -8 待付款 1：已付款  -1 申请退款 -2 退款中 -3 退款成功  0 已关闭 ')
    express_status = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(),
                               comment='快递状态， -7 待发货 -6 待收货 1 收货成功')
    express_address_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='快递地址id')
    express_info = db.Column(db.String(1000), nullable=False, server_default=db.FetchedValue(), comment='快递信息')
    comment_status = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(),
                               comment='评论状态  0 待评价 1：已评价')
    pay_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='付款到账时间')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最近一次更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')

    @property
    def order_status(self):
        return app.config['PAY_ORDER_STATUS'][str(self.status)]

    @property
    def pay_status(self):
        temp_status = self.status
        if self.status == 1:
            temp_status = self.express_status
            if self.express_status == 1 and self.comment_status == 0:
                temp_status = -5
            if self.express_status == 1 and self.comment_status == 1:
                temp_status = 1
        return temp_status

    @property
    def status_desc(self):
        return app.config['PAY_ORDER_STATUS_MAPPING'][str(self.pay_status)]

class PayOrderItem(BaseModel):
    __tablename__ = 'pay_order_item'

    id = db.Column(db.Integer, primary_key=True)
    pay_order_id = db.Column(db.Integer, nullable=False, index=True, server_default=db.FetchedValue(), comment='订单id')
    member_id = db.Column(db.BigInteger, nullable=False, server_default=db.FetchedValue(), comment='会员id')
    quantity = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='购买数量 默认1份')
    price = db.Column(db.Numeric(10, 2), nullable=False, server_default=db.FetchedValue(), comment='商品总价格，售价 * 数量')
    food_id = db.Column(db.Integer, nullable=False, index=True, server_default=db.FetchedValue(), comment='美食表id')
    note = db.Column(db.Text, nullable=False, comment='备注信息')
    status = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='状态：1：成功 0 失败')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最近一次更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')

    @property
    def status_desc(self):
        status_mapping = {'1':'成功','0':'失败'}
        return status_mapping[str(self.status)]

class PayOrderCallbackData(BaseModel):
    __tablename__ = 'pay_order_callback_data'

    id = db.Column(db.Integer, primary_key=True)
    pay_order_id = db.Column(db.Integer, nullable=False, unique=True, server_default=db.FetchedValue(),
                             comment='支付订单id')
    pay_data = db.Column(db.Text, nullable=False, comment='支付回调信息')
    refund_data = db.Column(db.Text, nullable=False, comment='退款回调信息')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后一次更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='创建时间')
