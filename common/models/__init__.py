from application import db

class BaseModel(db.Model):
    """模型基类"""
    __abstract__ = True
    def save(self):
        session = db.session
        session.add(self)
        session.commit()

    def delete(self):
        session = db.session
        session.delete(self)
        session.commit()

    def update(self):
        session = db.session
        session.commit()