# coding: utf-8
from common.models import BaseModel
from application import db
from application import app

class Food(BaseModel):
    __tablename__ = 'food'

    id = db.Column(db.Integer, primary_key=True)
    cat_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='分类id')
    name = db.Column(db.String(100), nullable=False, server_default=db.FetchedValue(), comment='美食名称')
    price = db.Column(db.Numeric(10, 2), nullable=False, server_default=db.FetchedValue(), comment='售卖金额')
    main_image = db.Column(db.String(100), nullable=False, server_default=db.FetchedValue(), comment='主图')
    summary = db.Column(db.String(10000), nullable=False, server_default=db.FetchedValue(), comment='描述')
    stock = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='库存量')
    tags = db.Column(db.String(200), nullable=False, server_default=db.FetchedValue(), comment='tag关键字，以","连接')
    status = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='状态 1：正常 0：删除')
    month_count = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='月销售数量')
    total_count = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='总销售量')
    view_count = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='总浏览次数')
    comment_count = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='总评论量')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后插入时间')


class FoodCat(BaseModel):
    __tablename__ = 'food_cat'

    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(50), nullable=False, server_default=db.FetchedValue(), comment='类别名称')
    weight = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='权重')
    status = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='状态 1：正常 0：已删除')
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='最后一次更新时间')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')

    @property
    def status_desc(self):
        return app.config['STATUS_MAPPING'][str(self.status)]


class FoodSaleChangeLog(BaseModel):
    __tablename__ = 'food_sale_change_log'

    id = db.Column(db.Integer, primary_key=True)
    food_id = db.Column(db.Integer, nullable=False, index=True, server_default=db.FetchedValue(), comment='商品id')
    quantity = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='售卖数量')
    price = db.Column(db.Numeric(10, 2), nullable=False, server_default=db.FetchedValue(), comment='售卖金额')
    member_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='会员id')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='售卖时间')


class FoodStockChangeLog(BaseModel):
    __tablename__ = 'food_stock_change_log'

    id = db.Column(db.Integer, primary_key=True)
    food_id = db.Column(db.Integer, nullable=False, index=True, comment='商品id')
    unit = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='变更多少')
    total_stock = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='变更之后总量')
    note = db.Column(db.String(100), nullable=False, server_default=db.FetchedValue(), comment='备注字段')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='插入时间')


class WxShareHistory(BaseModel):
    __tablename__ = 'wx_share_history'

    id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), comment='会员id')
    share_url = db.Column(db.String(200), nullable=False, server_default=db.FetchedValue(), comment='分享的页面url')
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue(), comment='创建时间')
