from common.models.queue import QueueList
from common.libs.Helper import getCurrentDate
import json


class QueueService(object):
    """
    数据库入队操作
    """
    @staticmethod
    def addQueue(queue_name,data=None):
        model_queue = QueueList()
        model_queue.queue_name = queue_name
        if data:
            model_queue.data = json.dumps(data)
        model_queue.status = -1
        model_queue.updated_time = getCurrentDate()
        model_queue.created_time = getCurrentDate()
        model_queue.save()

