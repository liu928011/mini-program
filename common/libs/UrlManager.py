import time,os
from application import app

class UrlManager(object):
    """
    url统一资源管理器
    """
    @staticmethod
    def buildUrl(path):
        return path

    @staticmethod
    def buildStaticUrl(path):
        release_version = app.config.get('RELEASE_VERSION')
        # 开发环境下，时间戳作为发行版本，避免浏览器缓存
        version = str(int(time.time())) if not release_version else release_version
        path = '/static'+path+'?version='+version
        return UrlManager.buildUrl(path)

    @staticmethod
    def buildImageUrl(file_key):
        app_config = app.config['APP']
        image_config = app.config['UPLOAD_IMAGES']
        return os.path.join(
            app_config['domain'],image_config['image_root'],file_key
        ).replace('\\','/')






