from common.models.log import AppAccessLog
from common.models.log import AppErrorLog
from common.libs.Helper import getCurrentDate
from flask import request,g
import json


class UserLogs(object):
    """
    访问日志记录
    """
    @staticmethod
    def access_log():
        access = AppAccessLog()
        access.referer_url = request.referrer
        access.target_url = request.url
        access.query_params = json.dumps(request.values.to_dict())
        access.ua = request.user_agent
        access.ip = request.remote_addr
        if 'current_user'in g and g.current_user is not None:
            access.uid = g.current_user.uid
        access.created_time = getCurrentDate()
        access.save()

    @staticmethod
    def error_log(error):
        """
        错误日志记录
        :param error:
        :return:
        """
        access = AppErrorLog()
        access.referer_url = request.referrer
        access.target_url = request.url
        access.query_params = json.dumps(request.values.to_dict())
        access.content=error
        access.created_time = getCurrentDate()
        access.save()
