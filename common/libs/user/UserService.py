import base64
import hashlib
import string
import random

class UserService:

    @staticmethod
    def geneAuthCode(user=None):
        # 生成授权码cookie
        str = '{}-{}-{}'.format(user.login_name, user.login_pwd, user.login_salt)
        md5 = hashlib.md5()
        md5.update(str.encode('utf-8'))
        return md5.hexdigest()

    @staticmethod
    def genePwd(pwd, salt):
        #加密算法 base64+md5+盐
        md5 = hashlib.md5(salt.encode())  # 加盐
        password = str(base64.encodebytes(pwd.encode('utf-8')))
        md5.update(password.encode('utf-8'))
        return md5.hexdigest()

    @staticmethod
    def geneSalt():
        # 随机生成12含有数字或字母的盐值
        return ''.join(random.choices(string.ascii_letters+string.digits,k=12))

