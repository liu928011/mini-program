from common.libs.Helper import getCurrentDate
from common.models.food import FoodStockChangeLog
from common.models.food import Food

def ChangeFoodStockLog(food_id, change_number, note):
    """
    库存变更记录
    :param food_id:
    :param change_number:
    :param note:
    :return:
    """
    if food_id < 1:
        return False
    food_info = Food.query.filter_by(id=food_id).first()
    if not food_info:
        return False

    food_stock = FoodStockChangeLog()
    food_stock.food_id = food_id
    food_stock.unit = change_number
    food_stock.total_stock = food_info.stock
    food_stock.note = note
    food_stock.created_time = getCurrentDate()
    food_stock.save()

    return True
