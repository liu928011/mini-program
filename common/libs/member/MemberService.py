import hashlib,string,random,requests,json
from application import app

class MemberService:

    @staticmethod
    def geneAuthCode(member=None):
        str = '{}-{}-{}'.format(member.id, member.nickname, member.salt)
        md5 = hashlib.md5()
        md5.update(str.encode('utf-8'))
        return md5.hexdigest()

    @staticmethod
    def geneSalt():
        # 随机生成12含有数字或字母的盐值
        return ''.join(random.choices(string.ascii_letters+string.digits,k=12))

    @staticmethod
    def getWetChatOpenId(code):
        url = 'https://api.weixin.qq.com/sns/jscode2session?appid={}&secret={}&js_code={}&grant_type=authorization_code'.format(
            app.config['MINA_INFO']['appid'], app.config['MINA_INFO']['secret'], code
        )
        # 请求，获取用户唯一标识openid
        response = requests.get(url=url)
        res = json.loads(response.text)
        openid = res['openid']
        return openid

